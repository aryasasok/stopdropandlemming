//
//  GameScene.swift
//  stopdropandlemming
//
//  Created by Arya S  Asok on 2019-06-19.
//  Copyright © 2019 Parrot. All rights reserved.
//




// mario - 1
  //ground - 2
//collision mask - 3
//*import SpriteKit
import GameplayKit

class GameScene: SKScene {
    let mario = SKSpriteNode(imageNamed: "mario")
    
    var entrance:SKNode!
    var exit:SKNode!
    var lastUpdateTime: TimeInterval = 0
    var dt: TimeInterval = 0

     override func didMove(to view: SKView) {
        self.entrance = self.childNode(withName: "ground")
        self.exit = self.childNode(withName: "ground1")
        
        // THE GAME SCENE
        // ---------------------
        // set the physics properties of this world
        
        
    }
  
    
     func spawnSand() {
     // 1. create a sand node
     let sandParticle = SKSpriteNode(imageNamed: "mario")
     
     // 2. set the initial position of the sand
     sandParticle.position.x = self.size.width/2      //  x = w/2
     sandParticle.position.y = self.size.height - 100 // y = h-100
     
     // 3. add physics body to sand
     sandParticle.physicsBody = SKPhysicsBody(circleOfRadius:sandParticle.size.width/2)
     
     // By default, dynamic = true, gravity = true
     // You don't need to change the defaults because
     // we  want the sand to fall down & collide into things
     
     // 3a. Add a "bounce" to your sand
     sandParticle.physicsBody?.restitution = 1.0
     
     // 4. add the sand to the scene
     addChild(sandParticle)
     }
     
     override func update(_ currentTime: TimeInterval) {
        self.spawnSand()
     }
    

    func makePlatform(xPosition:CGFloat, yPosition:CGFloat) {
        
        let platform = SKSpriteNode(imageNamed: "platform")
        platform.setScale(0.25)
        
        platform.position.x = xPosition;
        platform.position.y = yPosition;
        
        
        
        platform.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: platform.size.width, height: platform.size.width))
        platform.physicsBody?.affectedByGravity = false
        platform.physicsBody?.categoryBitMask = 4
        platform.physicsBody?.contactTestBitMask = 1
        platform.physicsBody?.isDynamic = false
        platform.physicsBody?.pinned = false
        
        
        addChild(platform)
        
    }
    func didBegin(_ contact: SKPhysicsContact) {
        
        
        let nodeA = contact.bodyA.node
        let nodeB = contact.bodyB.node
        
        print("you win")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        // 1. detect where the person clicked
        let touch = touches.first!
        let mousePosition = touch.location(in:self)
        
        makePlatform(xPosition: mousePosition.x, yPosition: mousePosition.y)
        // 2b. If person click screen
    } // end touchesBegan code
    
}
